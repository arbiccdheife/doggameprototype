package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;

public class Coin extends GameObject {

	Texture tex = Game.getInstance();
	private Animation animation;
	
	public Coin(float x, float y, ObjectId id) {
		super(x, y, id);
		this.width = 22;
		this.height = 20;
		animation = new Animation(5, tex.coins[0], tex.coins[1],tex.coins[2],tex.coins[3],tex.coins[4],tex.coins[5],tex.coins[6],tex.coins[7],tex.coins[8],tex.coins[9]);
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		animation.runAnimation();
	}

	@Override
	public void render(Graphics g) {
		animation.drawAnimation(g, (int)x, (int)y);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
	}

	

}
