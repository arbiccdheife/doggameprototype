package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.GameState;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Sounds;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class Player extends GameObject {
	private float gravity = .5f;
	private final float MAX_SPEED = 10;

	private int level;

	private int coinsCollected;
	private int lives;
	private int startX;
	private int startY;
	private boolean atEndOfLevel;

	private Handler handler;
	Texture tex = Game.getInstance();
	Sounds sounds = Game.getSounds();
	// AudioPlayer ap = Game.getAudioPlayer();

	private Animation playerWalkRight, playerWalkLeft;

	public Player(float x, float y, int level, Handler handler, ObjectId id) {
		super(x, y, id);
		this.width = 32;
		this.height = 64;
		this.handler = handler;
		this.level = level;
		setFacing(1);
		coinsCollected = 0;
		lives = 3;

		playerWalkRight = new Animation(10, tex.player[1], tex.player[2],
				tex.player[3], tex.player[4]);
		playerWalkLeft = new Animation(10, tex.player[6], tex.player[7],
				tex.player[8], tex.player[9]);
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		x += velX;
		y += velY;

		if (falling || jumping) {
			velY += gravity;
			if (velY > MAX_SPEED)
				velY = MAX_SPEED;
		}
		// System.out.println(y);
		// if (y > Game.HEIGHT*32) {
		// playerDeath();
		// }
		Collision(object);
		playerWalkLeft.runAnimation();
		playerWalkRight.runAnimation();
		if (coinsCollected >= 100) {
			lives++;
			coinsCollected = 0;
		}
	}

	@Override
	public void render(Graphics g) {
		// g.setColor(Color.blue);
		// g.fillRect((int)x, (int)y, (int)width, (int)height);
		if (!Game.getTransition().transitionIsInProgress()) {
			if (facing == 1) {
				if (jumping) {
					g.drawImage(tex.player_jump[0], (int) x, (int) y, null);
				} else if (!isWalking) {
					g.drawImage(tex.player[0], (int) x, (int) y, null);
				} else {
					playerWalkRight.drawAnimation(g, (int) x, (int) y);
				}
			} else if (facing == -1) {
				if (jumping) {
					g.drawImage(tex.player_jump[1], (int) x, (int) y, null);
				} else if (!isWalking) {
					g.drawImage(tex.player[5], (int) x, (int) y, null);
				} else {
					playerWalkLeft.drawAnimation(g, (int) x, (int) y);
				}
			}
		} else {
			if (facing == 1)
				g.drawImage(tex.player[0], (int) x, (int) y, null);
			if (facing == -1)
				g.drawImage(tex.player[5], (int) x, (int) y, null);
		}
		// Graphics2D g2d = (Graphics2D) g;
		// g2d.setColor(Color.red);
		// g2d.draw(getBounds());
		// g2d.draw(getBoundsLeft());
		// g2d.draw(getBoundsRight());
		// g2d.draw(getBoundsTop());

	}

	private void Collision(LinkedList<GameObject> object) {
		for (int i = 0; i < object.size(); i++) {
			GameObject tempObject = handler.objects.get(i);
			if (tempObject.getId() == ObjectId.Block) {
				if (getBoundsTop().intersects(tempObject.getBounds())) {
					y = tempObject.getY() + height / 2;
					velY = 0;
				}
				if (getBounds().intersects(tempObject.getBounds())) {
					y = tempObject.getY() - height + 1;
					velY = 0;
					falling = false;
					jumping = false;
					if (!isWalking)
						velX = 0;
				} else {
					falling = true;
				}

				if (getBoundsRight().intersects(tempObject.getBounds())) {
					x = tempObject.getX() - width;
				}

				if (getBoundsLeft().intersects(tempObject.getBounds())) {
					x = tempObject.getX() + width;
				}
			}

			if (tempObject.getId() == ObjectId.FleaCannon) {
				if (getBoundsTop().intersects(tempObject.getBounds())) {
					y = tempObject.getY() + tempObject.getHeight();
					velY = 0;
				}
				if (getBounds().intersects(tempObject.getBounds())) {
					y = tempObject.getY() - height + 1;
					velY = 0;
					falling = false;
					jumping = false;
					if (!isWalking)
						velX = 0;
				} else {
					falling = true;
				}

				if (getBoundsRight().intersects(tempObject.getBounds())) {
					x = tempObject.getX() - width - 3;
				}

				if (getBoundsLeft().intersects(tempObject.getBounds())) {
					x = tempObject.getX() + tempObject.getWidth() + 1;
				}
			}

			if (tempObject.getId() == ObjectId.MovingPlatform) {
				if (getBoundsTop().intersects(tempObject.getBounds())) {
					y = tempObject.getY() + height / 2;
					velY = 0;
				}
				if (getBounds().intersects(tempObject.getBounds())) {
					y = tempObject.getY() - height + 1;
					velY = 0;
					if (!isWalking) {
						velX = tempObject.getVelX();
					}
					falling = false;
					jumping = false;
				} else {
					falling = true;
				}

				if (getBoundsRight().intersects(tempObject.getBounds())) {
					x = tempObject.getX() - width;
				}

				if (getBoundsLeft().intersects(tempObject.getBounds())) {
					x = tempObject.getX() + tempObject.getWidth();
				}
			}

			if (tempObject.getId() == ObjectId.Coin) {
				if (getBoundsTop().intersects(tempObject.getBounds())
						|| getBounds().intersects(tempObject.getBounds())
						|| getBoundsRight().intersects(tempObject.getBounds())
						|| getBoundsLeft().intersects(tempObject.getBounds())) {

					// AudioPlayer.playSound(Audio.COIN);
					AudioPlayer.playSound(Audio.COIN);
					handler.removeObject(tempObject);
					coinsCollected++;
				}

			}
			if (tempObject.getId() == ObjectId.LevelEnd) {
				if (getBoundsTop().intersects(tempObject.getBounds())
						|| getBounds().intersects(tempObject.getBounds())
						|| getBoundsRight().intersects(tempObject.getBounds())
						|| getBoundsLeft().intersects(tempObject.getBounds())) {
					atEndOfLevel = true;
				} else {
					atEndOfLevel = false;
				}

			}
			if (tempObject.getId() == ObjectId.Flea) {

				Flea f = (Flea) tempObject;

				if (getBoundsFull().intersects(f.getBoundsTopEnemy())
						&& !getBoundsFull()
								.intersects(f.getBoundsBottomEnemy())) {
					handler.removeObject(f);
				} else if (getBoundsTop().intersects(tempObject.getBounds())
						|| getBoundsRight().intersects(tempObject.getBounds())
						|| getBoundsLeft().intersects(tempObject.getBounds())) {
					playerDeath();
				}

			}

			if (tempObject.getId() == ObjectId.Flyer) {

				Flyer f = (Flyer) tempObject;
				if (f.isDead()) continue;
				

				if (getBoundsFull().intersects(f.getBoundsTopEnemy())
						&& !getBoundsFull()
								.intersects(f.getBoundsBottomEnemy())) {
					//handler.removeObject(f);
					f.setIsDead(true);
				} else if (getBoundsTop().intersects(tempObject.getBounds())
						|| getBoundsRight().intersects(tempObject.getBounds())
						|| getBoundsLeft().intersects(tempObject.getBounds())) {
					playerDeath();
				}

			}
			if (tempObject.getId() == ObjectId.Liquid) {
				if (getBounds().intersects(tempObject.getBounds())) {
					playerDeath();
				}

			}

		}
	}

	public boolean isAtEndOfLevel() {
		return atEndOfLevel;
	}

	public void setAtEndOfLevel(boolean value) {
		atEndOfLevel = value;
	}

	public void playerDeath() {
		Game.getTransition().startTransition();
		lives--;
		x = startX;
		velX = 0;
		jumping = true;
		falling = true;
		y = startY;
		if (lives <= 0) {
			Game.setState(GameState.Menu);
		}
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getStartX() {
		return startX;
	}

	public void setStartX(int startX) {
		this.startX = startX;
	}

	public int getStartY() {
		return startY;
	}

	public void setStartY(int startY) {
		this.startY = startY;
	}

	public int getCoinsCollected() {
		return coinsCollected;
	}

	public void setCoinsCollected(int coinsCollected) {
		this.coinsCollected = coinsCollected;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int) ((int) x + width / 2 - (width / 2) / 2),
				(int) ((int) y + height / 2), (int) width / 2, (int) height / 2);
	}

	public Rectangle getBoundsRight() {
		return new Rectangle((int) ((int) x + width - 5), (int) y + 5, 5,
				(int) height - 15);
	}

	public Rectangle getBoundsLeft() {
		return new Rectangle((int) x, (int) y + 5, 5, (int) height - 15);
	}

	public Rectangle getBoundsTop() {
		return new Rectangle((int) ((int) x + width / 2 - (width / 2) / 2),
				(int) y, (int) width / 2, (int) height / 2);
	}

	public Rectangle getBoundsFull() {
		return new Rectangle((int) x, (int) y, width, height);
	}

}
