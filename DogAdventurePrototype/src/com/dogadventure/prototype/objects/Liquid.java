package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;

public class Liquid extends GameObject {

	Texture tex = Game.getInstance();
	private int type;
	private Animation topLiquidAnimate;
	
	public Liquid(float x, float y, int type, ObjectId id) {
		super(x, y, id);
		this.type = type;
		this.width = 32;
		this.height = 32;
		
		if (type == 1) {
			//Top water...animate
			topLiquidAnimate = new Animation(50, tex.liquid[1], tex.liquid[2]);
		}
		
		if (type == 3) {
			//Top Lava...animate
			topLiquidAnimate = new Animation(50, tex.liquid[4], tex.liquid[5]);
		}
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		if (type == 1 || type == 3) topLiquidAnimate.runAnimation();
	}

	@Override
	public void render(Graphics g) {
		if (type == 0)
			g.drawImage(tex.liquid[0], (int)x, (int)y, null);
		else if (type == 1 || type == 3) {
			//animate
			topLiquidAnimate.drawAnimation(g, (int)x, (int)y);
		} else if (type == 2) {
			g.drawImage(tex.liquid[3], (int)x, (int)y, null);
		} 
		
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 32, 32);
	}

	

}
