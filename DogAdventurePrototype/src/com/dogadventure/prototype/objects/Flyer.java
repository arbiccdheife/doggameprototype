package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class Flyer extends GameObject {

	private final float FLYER_SPEED = -.7f;

	Texture tex = Game.getInstance();
	// private int type; //0 = slow, 1 = fast
	private Handler handler;

	private float gravity = .4f;
	private final float MAX_SPEED = 10;

	private int countSinceTurn = 10;
	private int upsSinceCaw = 0;
	private boolean isDead = false;
	private int ticksSinceDeath = 0;

	private Animation flyRight, flyLeft;

	public Flyer(float x, float y, int type, ObjectId id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		this.width = 32;
		this.height = 32;
		velX = FLYER_SPEED;
		facing = -1;
		isWalking = true;

		flyLeft = new Animation(10, tex.flyers[0], tex.flyers[1],
				tex.flyers[2], tex.flyers[3], tex.flyers[2], tex.flyers[1]);
		flyRight = new Animation(10, tex.flyers[4], tex.flyers[5],
				tex.flyers[6], tex.flyers[7], tex.flyers[6], tex.flyers[5]);
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		x += velX;
		y += velY;
		countSinceTurn++;

		if (!isDead) {
			// Bird caws if in screen.
			upsSinceCaw++;
			float camX = Game.getCamera().getX();
			if ((x + camX >= 0 && x + camX <= Game.WIDTH) && upsSinceCaw > 180) {
				upsSinceCaw = 0;
				AudioPlayer.playSound(Audio.CAW);
			}

			if (facing == -1)
				velX = FLYER_SPEED;
			else if (facing == 1)
				velX = -FLYER_SPEED;

			flyLeft.runAnimation();
			flyRight.runAnimation();
			collision(object);
		} else {
			velY += gravity;
			if (velY >= MAX_SPEED)
				velY = MAX_SPEED;
			ticksSinceDeath++;
			if (ticksSinceDeath > 180) {
				handler.removeObject(this);
			}
		}
	}

	@Override
	public void render(Graphics g) {

		// Graphics2D g2d = (Graphics2D)g;
		// g2d.setColor(Color.BLUE);
		// g2d.fill(getBounds());
		// g2d.setColor(Color.red);
		// g2d.draw(getBoundsBottomEnemy());
		// g2d.draw(getBoundsTopEnemy());
		if (!isDead) {
			if (facing == 1) {
				flyRight.drawAnimation(g, (int) x, (int) y);
			} else {
				flyLeft.drawAnimation(g, (int) x, (int) y);
			}
			if (upsSinceCaw <= 60) g.drawImage(tex.flyers[10], (int)(x+31), (int)(y-28), null);
		} else {
			if (facing == 1)
				g.drawImage(tex.flyers[9], (int) x, (int) y, null);
			else
				g.drawImage(tex.flyers[8], (int) x, (int) y, null);
		}

	}

	private void collision(LinkedList<GameObject> object) {
		boolean inContact = false;
		for (int i = 0; i < object.size(); i++) {
			GameObject tempObject = handler.objects.get(i);
			if (tempObject.getId() == ObjectId.Block
					|| tempObject.getId() == ObjectId.MobBlocker
					|| tempObject.getId() == ObjectId.FleaCannon) {

				if (countSinceTurn > 10
						&& (getBoundsRight().intersects(tempObject.getBounds()) || getBoundsLeft()
								.intersects(tempObject.getBounds()))) {
					countSinceTurn = 0;
					velX = -velX;
					if (facing == 1)
						facing = -1;
					else
						facing = 1;
				}

			}

			if (tempObject.getId() == ObjectId.Liquid) {
				if (getBoundsBottom().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}

			}

		}

		if (!inContact) {
			falling = true;
		}
	}

	public void setIsDead(boolean tf) {
		isDead = tf;
		ticksSinceDeath = 0;
	}

	public boolean isDead() {
		return isDead;
	}

	public Rectangle getBoundsBottom() {
		return new Rectangle((int) ((int) x + width / 2 - (width / 2) / 2),
				(int) ((int) y + height / 2), (int) width / 2, (int) height / 2);
	}

	public Rectangle getBoundsTop() {
		return new Rectangle((int) x + 3, (int) y, width - 3, 3);
	}

	public Rectangle getBoundsLeft() {
		return new Rectangle((int) x, (int) y + 2, 5, (int) height - 4);
	}

	public Rectangle getBoundsRight() {
		return new Rectangle((int) ((int) x + width - 5), (int) y + 10, 5,
				(int) height - 20);
	}

	public Rectangle getBounds() {
		return new Rectangle((int) x, (int) y, width, height);

	}

	public Rectangle getBoundsTopEnemy() {
		return new Rectangle((int) x, (int) y, width, 10);
	}

	public Rectangle getBoundsBottomEnemy() {
		return new Rectangle((int) x, (int) (y + height - 22), width, 22);
	}

}
