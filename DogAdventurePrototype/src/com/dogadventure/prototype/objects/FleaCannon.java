package com.dogadventure.prototype.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.util.LinkedList;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class FleaCannon extends GameObject {
	
	private final int TILE_SIZE = 32;

	private final int TIME_BETWEEN_SHOTS = 8;
	private final int RANGE_TO_FIRE = 300;

	Texture tex = Game.getInstance();
	private int type; //0 = left, 1 = right, 2 = up 3 = down
	private Handler handler;
	
	private int countSinceFire = 8;
	
	
	
	public FleaCannon(float x, float y, int type, ObjectId id, Handler handler) {
		super(x, y, id);
		this.type = type;
		this.handler = handler;
		
		if (type == 0 || type == 1) {
			this.width = 64;
			this.height = 32;
		} else {
			this.width = 32;
			this.height= 64;
		}
		
	
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		Float playerX = Game.getPlayer().getX();
	
		countSinceFire++;
		
		if ((this.x - playerX < RANGE_TO_FIRE && this.x - playerX > -RANGE_TO_FIRE) && (countSinceFire > 60 * TIME_BETWEEN_SHOTS)) {
			fireFlea();
			countSinceFire = 0;
		}
	
	}
	
	private void fireFlea() {
		
		Flea f;
		Double d;
		AudioPlayer.playSound(Audio.BLAST);
		switch (type) {
		
		case(0) :
			f = new Flea(x- TILE_SIZE, y, 0, ObjectId.Flea, handler);
			handler.addObject(f);
			
			f.setVelX(-8f);
			break;
		case(1) :
			f = new Flea(x + width, y, 0, ObjectId.Flea, handler);
			handler.addObject(f);
		
			f.setVelX(8f);
			f.setFacing(1);
			break;
		case(2) :
			f = new Flea(x, y - TILE_SIZE, 0, ObjectId.Flea, handler);
			handler.addObject(f);
			d = Math.random();
			f.setVelY(-15f);
			if (d < .5) {
				f.setVelX(-2f);
				f.setFacing(-1);
			} else {
				f.setVelX(2f);
				f.setFacing(1);
			}
	
			break;
		case(3) :
			f = new Flea(x, y + height, 0, ObjectId.Flea, handler);
			handler.addObject(f);
			f.setVelY(10f);
			
			d = Math.random();
			if (d < .5) {
				f.setFacing(-1);
			} else {
				f.setFacing(1);
			}
			break;
		default :
			
		}
	}

	@Override
	public void render(Graphics g) {
	
		switch (type) {
		case(0) :
			g.drawImage(tex.cannons[0], (int)x, (int)y, null);
			break;
		case(1) :
			g.drawImage(tex.cannons[1], (int)x, (int)y, null);
			break;
		case(2) :
			g.drawImage(tex.cannons[2], (int)x, (int)y, null);
			break;
		case(3) :
			g.drawImage(tex.cannons[3], (int)x, (int)y, null);
			break;
		default :
			
		}
	}
	




	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return new Rectangle((int)x, (int)y, width, height);
		
	}

	

}
