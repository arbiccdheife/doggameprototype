package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Game;

public class Scenery extends GameObject{
//	scenery[0] = bs.grabImage(2, 1, TILE_SIZE, TILE_SIZE); //Grass scenery
//	scenery[1] = bs.grabImage(3, 1, TILE_SIZE, TILE_SIZE); //Flower scenery
//	scenery[2] = bs.grabImage(5, 1, TILE_SIZE, TILE_SIZE); //Grass scenery
//	scenery[3] = bs.grabImage(8, 1, TILE_SIZE, TILE_SIZE); //Left grass right dirt
//	scenery[4] = bs.grabImage(9, 1, TILE_SIZE, TILE_SIZE); //Left dirt right dirt
//	scenery[5] = bs.grabImage(10, 1, TILE_SIZE, TILE_SIZE); //Left dirt right grass
//	scenery[6] = bs.grabImage(4, 2, TILE_SIZE, TILE_SIZE); //Left grass ending
//	scenery[7] = bs.grabImage(6, 2, TILE_SIZE, TILE_SIZE); //Right grass ending
//	scenery[8] = bs.grabImage(7, 2, TILE_SIZE, TILE_SIZE); //Left dirt ending
//	scenery[9] = bs.grabImage(9, 2, TILE_SIZE, TILE_SIZE); //Right dirt ending
//	scenery[10] = bs.grabImage(10, 2, TILE_SIZE, TILE_SIZE); //Left grass ending in dirt
//	scenery[11] = bs.grabImage(11, 2, TILE_SIZE, TILE_SIZE); //right grass ending in dirt
//	scenery[12] = bs.grabImage(13, 2, TILE_SIZE, TILE_SIZE); //Right/left grass ending in dirt
//	scenery[13] = bs.grabImage(14, 2, TILE_SIZE, TILE_SIZE); //Right/left grass endings

	private int type;
	
	/*TYPES:
	 * 0-grass scenery
	 * 1-flower
	 * 2-grass scenery
	 * 3-left grass ending right dirt ending
	 * 4-left dirt ending right dirt ending
	 * 5-left dirt ending right grass ending
	 * 6-left grass ending
	 * 7-right grass ending
	 * 8-left dirt ending
	 * 9-right dirt ending
	 * 10-left grass ending in dirt
	 * 11-right grass ending in dirt
	 * 12-right/left grass ending in dirt
	 * 13-right/left grass endings
	 * 
	 */
	
	Texture tex = Game.getInstance();

	public Scenery(float x, float y, int type, ObjectId id) {
		super(x, y, id);
		this.type = type;
		this.id = id;
	}

	@Override
	public void tick(LinkedList<GameObject> object) {}

	@Override
	public void render(Graphics g) {
		// TODO Auto-generated method stub
		g.drawImage(tex.scenery[type], (int)x, (int)y, null);
		
	}

	@Override
	public Rectangle getBounds() {
		// TODO Auto-generated method stub
		return null;
	}

}
