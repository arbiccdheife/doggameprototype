package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Game;

public class Block extends GameObject {
//	block[0] = bs.grabImage(5, 2, TILE_SIZE, TILE_SIZE); //Grass ground
//	block[1] = bs.grabImage(8, 2, TILE_SIZE, TILE_SIZE); //Earth ground
//	block[2] = bs.grabImage(1, 2, TILE_SIZE, TILE_SIZE); //Left platform
//	block[3] = bs.grabImage(2, 2, TILE_SIZE, TILE_SIZE); //Middle platform
//	block[4] = bs.grabImage(3, 2, TILE_SIZE, TILE_SIZE); //Right platform
//	block[5] = bs.grabImage(12, 2, TILE_SIZE, TILE_SIZE); //Single platform
	
	/* Types:
	 * 0 - grass ground
	 * 1 - earth ground
	 * 2 - left platform
	 * 3 - middle platform
	 * 4 - right platform
	 * 5 - single platform
	 * 6 - dirt bottom
	 * 7 - flat platform for cannon
	 * 8 - flat dirt for cannon
	 */

	Texture tex = Game.getInstance();
	private int type;
	
	public Block(float x, float y, int type, ObjectId id) {
		super(x, y, id);
		this.width = 32;
		this.height = 32;
		this.type = type;
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(tex.block[type], (int)x, (int)y, null);
		
		
//		if (type == 0)
//			g.drawImage(tex.block[0], (int)x, (int)y, null);
//		else if (type == 1)
//			g.drawImage(tex.block[1], (int)x , (int)y, null);
//		else if (type == 2)
//			g.drawImage(tex.block[2], (int)x, (int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
	}

	

}
