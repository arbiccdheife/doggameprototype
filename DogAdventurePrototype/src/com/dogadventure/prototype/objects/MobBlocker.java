package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;

public class MobBlocker extends GameObject{

	public MobBlocker(float x, float y, ObjectId id) {
		super(x, y, id);
		this.width = 32;
		this.height = 32;
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		
		
	}

	@Override
	public void render(Graphics g) {
//	Graphics2D g2d= (Graphics2D)g;
//	g2d.setColor(Color.RED);
//	g2d.draw(getBounds());
		
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
	}

}
