package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Animation;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class Flea extends GameObject {
	
	private final float FLEA_SPEED = -.7f;

	Texture tex = Game.getInstance();
	//private int type; //0 = slow, 1 = fast
	private Handler handler;
	private float gravity = .5f;
	private final float MAX_SPEED = 10;
	private int countSinceTurn = 10;
	
	private Animation fleaWalkRight, fleaWalkLeft;
	
	public Flea(float x, float y, int type, ObjectId id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		this.width = 32;
		this.height = 32;
		velX = FLEA_SPEED;
		facing = -1;
		isWalking = true;
		
		
		
		fleaWalkLeft = new Animation(17, tex.fleas[1], tex.fleas[2], tex.fleas[3], tex.fleas[4]);
		fleaWalkRight = new Animation(17, tex.fleas[6], tex.fleas[7], tex.fleas[8], tex.fleas[9]);
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		x += velX;
		y += velY;
		countSinceTurn++;
		
		if (falling) {
			velY += gravity;
			if (velY > MAX_SPEED)
				velY = MAX_SPEED;
		} else if (facing == -1) {
			velX = FLEA_SPEED;
		} else if (facing == 1) {
			velX = -FLEA_SPEED;
		}
		
		collision(object);
		fleaWalkLeft.runAnimation();
		fleaWalkRight.runAnimation();
	}

	@Override
	public void render(Graphics g) {
		if (falling) {
			if (facing == -1) g.drawImage(tex.fleas[0], (int)x, (int)y, null);
			else g.drawImage(tex.fleas[5], (int)x, (int)y, null);
		} else {
			if (facing == 1) {
				fleaWalkRight.drawAnimation(g, (int)x, (int)y);
			} else {
				fleaWalkLeft.drawAnimation(g, (int)x, (int)y);
			}
		}
//		Graphics2D g2d = (Graphics2D)g;
//		g2d.setColor(Color.red);
//		g2d.draw(getBoundsBottomEnemy());
//		g2d.draw(getBoundsTopEnemy());

	}
	
	private void collision(LinkedList<GameObject> object) {
		boolean inContact = false;
		for (int i = 0; i < object.size(); i++) {
			GameObject tempObject = handler.objects.get(i);
			if(tempObject.getId() == ObjectId.Block || tempObject.getId() == ObjectId.MobBlocker || tempObject.getId() == ObjectId.FleaCannon) {
				
				if (getBoundsTop().intersects(tempObject.getBounds())) {
					if (!getBoundsBottom().intersects(tempObject.getBounds()) &&!getBoundsLeft().intersects(tempObject.getBounds()) &&!getBoundsRight().intersects(tempObject.getBounds())) {
						y = tempObject.getY() + tempObject.getHeight();
						velY = 0;
						falling = true;
					} else {
						y = tempObject.getY() + height/2;
					}
				}
				if (getBoundsBottom().intersects(tempObject.getBounds())) {
					y = tempObject.getY() - height+1;
					velY = 0;
					falling = false;
					jumping = false;
					inContact = true;
				} 
				
				if (countSinceTurn > 10 && (getBoundsRight().intersects(tempObject.getBounds()) || getBoundsLeft().intersects(tempObject.getBounds())) && !falling) {
					countSinceTurn = 0;
					velX = -velX;
					if (facing == 1) facing = -1;
					else facing = 1;
				}
				
			}
			
			if(tempObject.getId() == ObjectId.Liquid) {
				if (getBoundsBottom().intersects(tempObject.getBounds())) {
					handler.removeObject(this);
				}
				
			}
			
				
		}
		
		if(!inContact) {
			falling = true;
		}
	}

	public Rectangle getBoundsBottom() {
		return new Rectangle((int)((int)x + width/2 - (width/2)/2), (int)((int)y + height/2), (int)width/2, (int)height/2);
	}
	
	public Rectangle getBoundsTop() {
		return new Rectangle ((int)x+3, (int)y, width - 3, 3);
	}

	public Rectangle getBoundsLeft() {
		return new Rectangle((int)x, (int)y + 10, 5, (int)height-20);
	}
	
	public Rectangle getBoundsRight() {
		return new Rectangle((int)((int)x+ width-5), (int)y + 10, 5, (int)height-20);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
		
	}
	
	public Rectangle getBoundsTopEnemy() {
		return new Rectangle((int)x, (int)y, width, 10);
	}
	
	public Rectangle getBoundsBottomEnemy() {
		return new Rectangle((int)x, (int)(y+height-22), width, 22);
	}

	

}
