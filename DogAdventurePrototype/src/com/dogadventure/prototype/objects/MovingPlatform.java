package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class MovingPlatform extends GameObject {

	Texture tex = Game.getInstance();
	
	private Handler handler;
	
	public MovingPlatform(float x, float y, int length, ObjectId id, Handler handler) {
		super(x, y, id);
		//this.length = length;
		this.handler = handler;
		this.width = 32 * length;
		this.height = 32;
		velX = -1.5f;
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		x += velX;
		collision(object);
	}

	@Override
	public void render(Graphics g) {
//		for (int i = 0; i < length; i++) {
//			g.drawImage(tex.block[2], (int)(x + 32*i), (int)y, null);
//		}
		g.drawImage(tex.movingPlatform[0], (int)(x), (int)y, null);
		g.drawImage(tex.movingPlatform[1], (int)(x+32), (int)y, null);
		g.drawImage(tex.movingPlatform[2], (int)(x+64), (int)y, null);
		
//		if (type == 0)
//			g.drawImage(tex.block[0], (int)x, (int)y, null);
//		else if (type == 1)
//			g.drawImage(tex.block[1], (int)x , (int)y, null);
//		else if (type == 2)
//			g.drawImage(tex.block[2], (int)x, (int)y, null);
	}
	
	private void collision(LinkedList<GameObject> object) {
		for (int i = 0; i < object.size(); i++) {
			GameObject tempObject = handler.objects.get(i);
			if(tempObject.getId() == ObjectId.Block || tempObject.getId() == ObjectId.MovingPlatform) {
				
				if (getBoundsRight().intersects(tempObject.getBounds())) {
					velX = -velX;
				}
				
				if (getBoundsLeft().intersects(tempObject.getBounds())) {
					velX = -velX;
				}
			}
		}
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
	}
	
	public Rectangle getBoundsLeft() {
		return new Rectangle((int)x, (int)y, width/2, height);
	}
	public Rectangle getBoundsRight() {
		return new Rectangle((int)(x + width/2), (int)y, width/2, height);
	}

	

}
