package com.dogadventure.prototype.objects;

import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.window.Game;

public class LevelEnd extends GameObject {

	Texture tex = Game.getInstance();
	
	
	public LevelEnd(float x, float y, ObjectId id) {
		super(x, y, id);
		this.width = 32;
		this.height = 32;
	}

	@Override
	public void tick(LinkedList<GameObject> object) {
		
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(tex.randoms[0], (int)x, (int)y, null);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, width, height);
	}

	

}
