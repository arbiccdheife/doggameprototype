package com.dogadventure.prototype.framework;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Handler;

public class KeyInput extends KeyAdapter {
	Handler handler;
	private boolean spacePressed = false;
	private boolean escPressed = false;

	public KeyInput(Handler handler) {
		this.handler = handler;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (Game.getState() == GameState.Game) {
			for (int i = 0; i < handler.objects.size(); i++) {
				GameObject tempObject = handler.objects.get(i);
				if (tempObject.getId() == ObjectId.Player) {
					if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
						tempObject.setWalking(true);
						tempObject.setVelX(5);
						tempObject.setFacing(1);
					}
					if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
						tempObject.setWalking(true);
						tempObject.setVelX(-5);
						tempObject.setFacing(-1);
					}
					if (key == KeyEvent.VK_SPACE && !tempObject.isJumping()
							&& !spacePressed) {
						spacePressed = true;
						tempObject.setJumping(true);
						tempObject.setFalling(true);
						tempObject.setVelY(-10);
//						sounds.sounds[4].setFramePosition(0);
//						sounds.sounds[4].start();
						AudioPlayer.playSound(Audio.JUMP);
					}
				}
			}
			if (key == KeyEvent.VK_ESCAPE && !escPressed) {
				Game.setState(GameState.Pause);
				escPressed = true;
			}
		} else if (Game.getState() == GameState.Menu) {
			if (key == KeyEvent.VK_ESCAPE && !escPressed) {
				System.exit(1);
			}
		} else if (Game.getState() == GameState.Pause) {
			if (key == KeyEvent.VK_ESCAPE && !escPressed) {
				//System.exit(1);
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_ESCAPE) {
			escPressed = false;
		}

		for (int i = 0; i < handler.objects.size(); i++) {
			GameObject tempObject = handler.objects.get(i);
			if (tempObject.getId() == ObjectId.Player) {
				if (key == KeyEvent.VK_D || key == KeyEvent.VK_RIGHT) {
					tempObject.setWalking(false);
					tempObject.setVelX(0);
				}
				if (key == KeyEvent.VK_A || key == KeyEvent.VK_LEFT) {
					tempObject.setWalking(false);
					tempObject.setVelX(0);
				}
				if (key == KeyEvent.VK_SPACE)
					spacePressed = false;
			}
		}
	}
}
