package com.dogadventure.prototype.framework;

public enum ObjectId {
	
	Player(),
	Block(),
	Liquid(),
	MovingPlatform(),
	Coin(),
	LevelEnd(),
	Flea(),
	Scenery(),
	MobBlocker(),
	FleaCannon(),
	Flyer()
};
