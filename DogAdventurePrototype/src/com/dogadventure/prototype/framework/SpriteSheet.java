package com.dogadventure.prototype.framework;

import java.awt.image.BufferedImage;

public class SpriteSheet {
	private final int COLUMN_WIDTH = 32;
	
	private final int ROW_WIDTH = 32;

	private BufferedImage image;
	
	private int rowHeight, columnWidth;
	
	public SpriteSheet(BufferedImage image, int rowHeight, int colWidth) {
		this.image = image;
		this.rowHeight = rowHeight;
		this.columnWidth = colWidth;
	}
	
	public BufferedImage grabImage(int col, int row, int width, int height) {
		BufferedImage img = image.getSubimage((col * columnWidth) - columnWidth , row*rowHeight - rowHeight,
				width, height);
		return img;
	}

}
