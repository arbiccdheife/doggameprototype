package com.dogadventure.prototype.framework;

public enum GameState {
	Menu,
	Game,
	Pause,
	LevelSelect
};
