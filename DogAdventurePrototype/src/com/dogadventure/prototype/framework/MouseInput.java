package com.dogadventure.prototype.framework;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JOptionPane;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.window.Game;
import com.dogadventure.prototype.window.Window;

public class MouseInput implements MouseListener {

	Sounds sounds = Game.getSounds();
	@Override
	public void mouseClicked(MouseEvent e) { }

	@Override
	public void mousePressed(MouseEvent e) {
		int mx = e.getX();
		int my = e.getY();
		
		if (mx >= 15 && mx <= 45) {
			if (my >= 15 && my <= 45) {
				System.out.println("toggle music");
				Game.toggleMusic();
			}
		}
			
		

		if (Game.getState() == GameState.Menu) {
			if (mx >= 310 && mx <= 500) {
				if (my >= 200 && my <= 249) {
					// Play Button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getMenu().setPlayIsPressed(true);
				} else if (my >= 300 && my <= 349) {
					// Level select button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getMenu().setLevelSelectIsPressed(true);
				}else if (my >= 400 && my <= 449) {
					// Quit Button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getMenu().setQuitIsPressed(true);
				}
			}
		} else if (Game.getState() == GameState.Pause) {
			if (mx >= 310 && mx <= 500) {
				if (my >= 200 && my <= 249) {
					// Resume Button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getPauseMenu().setResumeIsPressed(true);
				} else if (my >= 300 && my <= 349) {
					// LevelSelect Button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getPauseMenu().setLevelSelectIsPressed(true);
				} else if (my >= 400 && my <= 449) {
					// Quit Button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getPauseMenu().setQuitIsPressed(true);
				}
			}
		} else if (Game.getState() == GameState.LevelSelect) {
			if (my >= 400 && my <= 449) {
				if (mx >= 100 && mx <= 290) {
					//first button
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getLevelSelect().setFirstButtonPressed(true);
				} else if ((Game.getLevelSelect().getCurrentPage()-1) * 3 + 2 <= Game.getLevelSelect().getNumberOfLevels() && mx >= 310 && mx <= 500) {
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getLevelSelect().setSecondButtonPressed(true);
				} else if ((Game.getLevelSelect().getCurrentPage()-1) * 3 + 3 <= Game.getLevelSelect().getNumberOfLevels() && mx >= 520 && mx <= 710) {
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getLevelSelect().setThirdButtonPressed(true);
				}
			}
			
			if (Game.getLevelSelect().getCurrentPage() < Game.getLevelSelect().getNumberOfPages())  {
				if (mx >= 750 && mx <= 780 && my >= 300 && my <= 330) {
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getLevelSelect().setRightButtonPressed(true);
				}
			}
			
			if (Game.getLevelSelect().getCurrentPage() > 1)  {
				if (mx >= 50 && mx <= 80 && my >= 300 && my <= 330) {
					AudioPlayer.playSound(Audio.BUTTON_PRESS);
					Game.getLevelSelect().setLeftButtonPressed(true);
				}
			}
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if (Game.getState() == GameState.Game)
			return;
		int mx = e.getX();
		int my = e.getY();

		if (Game.getState() == GameState.Menu) {
			if (mx >= 310 && mx <= 500) {
				if (Game.getMenu().isPlayIsPressed() && my >= 200 && my <= 249) {
					// Play Button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					Game.getTransition().startTransition();
					Game.setState(GameState.Game);
					Window.getGame().newGame(1);
					
				} else if (Game.getMenu().levelSelectIsPressed() && my >= 300
						&& my <= 349) {
					// Level select button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					Game.getTransition().startTransition();
					Game.setState(GameState.LevelSelect);
					//JOptionPane.showMessageDialog(null, "Not yet implemented!");
				}else if (Game.getMenu().isQuitIsPressed() && my >= 400
						&& my <= 449) {
					// Quit Button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.exit(1);
				}
			}
			
			Game.getMenu().setPlayIsPressed(false);
			Game.getMenu().setLevelSelectIsPressed(false);
			Game.getMenu().setQuitIsPressed(false);
		} else if (Game.getState() == GameState.Pause) {
			if (mx >= 310 && mx <= 500) {
				if (Game.getPauseMenu().isResumeIsPressed() && my >= 200 && my <= 249) {
					// Resume Button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					Game.setState(GameState.Game);
					
				} else if (Game.getPauseMenu().levelSelectIsPressed() && my >= 300
						&& my <= 349) {
					// Level select button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					Game.getTransition().startTransition();
					Game.setState(GameState.LevelSelect);
				} else if (Game.getPauseMenu().isQuitIsPressed() && my >= 400
						&& my <= 449) {
					// Quit Button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.exit(1);
				}
			}
			Game.getPauseMenu().setResumeIsPressed(false);
			Game.getPauseMenu().setLevelSelectIsPressed(false);
			Game.getPauseMenu().setQuitIsPressed(false);
		} else if (Game.getState() == GameState.LevelSelect) {
			if (my >= 400 && my <= 449) {
				if (Game.getLevelSelect().firstButtonPressed() && (mx >= 100 && mx <= 290)) {
					//first button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.out.println("First level selected");
					Game.getTransition().startTransition();
					Game.setState(GameState.Game);
					Window.getGame().newGame((Game.getLevelSelect().getCurrentPage()-1)*3+1);
				} else if ((Game.getLevelSelect().getCurrentPage()-1) * 3 + 2 <= Game.getLevelSelect().getNumberOfLevels() && Game.getLevelSelect().secondButtonPressed() && (mx >= 310 && mx <= 500)) {
					//second button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.out.println("Second level selected");
					Game.getTransition().startTransition();
					Game.setState(GameState.Game);
					Window.getGame().newGame((Game.getLevelSelect().getCurrentPage()-1)*3+2);
					
				} else if ((Game.getLevelSelect().getCurrentPage()-1) * 3 + 3 <= Game.getLevelSelect().getNumberOfLevels() && Game.getLevelSelect().thirdButtonPressed() && (mx >= 520 && mx <= 710)) {
					//third button
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.out.println("Third level selected");
					Game.getTransition().startTransition();
					Game.setState(GameState.Game);
					Window.getGame().newGame((Game.getLevelSelect().getCurrentPage()-1)*3+3);
				}
			}
			
			if (Game.getLevelSelect().getCurrentPage() < Game.getLevelSelect().getNumberOfPages())  {
				if (Game.getLevelSelect().rightButtonPressed() && (mx >= 750 && mx <= 780 && my >= 300 && my <= 330)) {
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.out.println("Right arrow selected");
					Game.getLevelSelect().setCurrentPage(Game.getLevelSelect().getCurrentPage()+1);
					
				}
			}
			
			if (Game.getLevelSelect().getCurrentPage() > 1)  {
				if (Game.getLevelSelect().leftButtonPressed() && (mx >= 50 && mx <= 80 && my >= 300 && my <= 330)) {
					AudioPlayer.playSound(Audio.BUTTON_RELEASE);
					System.out.println("Left arrow selected");
					Game.getLevelSelect().setCurrentPage(Game.getLevelSelect().getCurrentPage()-1);
					
				}
			}
			
			Game.getLevelSelect().setFirstButtonPressed(false);
			Game.getLevelSelect().setSecondButtonPressed(false);
			Game.getLevelSelect().setThirdButtonPressed(false);
			Game.getLevelSelect().setRightButtonPressed(false);
			Game.getLevelSelect().setLeftButtonPressed(false);
		}
	}
	
//	if (!firstIsPressed) {
//		g.drawImage(buttonUnpressed, 100, 400, null);
//		g.drawString(Integer.toString(currentPage), 190, 435);
//	} else {
//		g.drawImage(buttonPressed, 100, 404, null);
//		g.drawString(Integer.toString(currentPage), 190, 439);
//	}
//	
//	if (!secondIsPressed) {
//		g.drawImage(buttonUnpressed, 310, 400, null);
//		g.drawString(Integer.toString(currentPage+1), 400, 435);
//	} else {
//		g.drawImage(buttonPressed, 310, 404, null);
//		g.drawString(Integer.toString(currentPage+1), 400, 439);
//	}
//	
//	if (!thirdIsPressed) {
//		g.drawImage(buttonUnpressed, 520, 400, null);
//		g.drawString(Integer.toString(currentPage+2), 605, 435);
//	} else {
//		g.drawImage(buttonPressed, 520, 404, null);
//		g.drawString(Integer.toString(currentPage+2), 605, 439);
//	}

	@Override
	public void mouseEntered(MouseEvent e) { }

	@Override
	public void mouseExited(MouseEvent e) { }

}
