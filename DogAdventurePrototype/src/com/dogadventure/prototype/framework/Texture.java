package com.dogadventure.prototype.framework;

import java.awt.image.BufferedImage;

import com.dogadventure.prototype.window.ResourceLoader;

public class Texture {
	private final int TILE_SIZE = 32;


	SpriteSheet bs, ps, cs, rs;
	private BufferedImage block_sheet = null;
	private BufferedImage player_sheet = null;
	private BufferedImage coin_sheet = null;
	private BufferedImage random_sheet = null;
	
	
	public BufferedImage[] block = new BufferedImage[9];
	public BufferedImage[] scenery = new BufferedImage[14];
	public BufferedImage[] movingPlatform = new BufferedImage[3];
	
	public BufferedImage[] liquid = new BufferedImage[6];
	public BufferedImage[] coins = new BufferedImage[10];
	public BufferedImage[] randoms = new BufferedImage[1];
	public BufferedImage[] fleas = new BufferedImage[10];
	public BufferedImage[] flyers = new BufferedImage[11];
	public BufferedImage[] cannons = new BufferedImage[4];
	
	public BufferedImage[] player = new BufferedImage[10];
	public BufferedImage[] player_jump = new BufferedImage[2];
	
	public Texture() {
		ResourceLoader loader = new ResourceLoader();
		
		block_sheet = loader.loadImage("/newTiles.gif");
		player_sheet = loader.loadImage("/player2.png");
		coin_sheet = loader.loadImage("/coins.png");
		random_sheet = loader.loadImage("/random_things.png");
		
		bs = new SpriteSheet(block_sheet, 32, 32);
		ps = new SpriteSheet(player_sheet, 64, 32);
		cs = new SpriteSheet(coin_sheet, 20, 22);
		rs = new SpriteSheet(random_sheet,32,32);
		getTextures();
	}
	
	private void getTextures() {
		
		block[0] = bs.grabImage(5, 2, TILE_SIZE, TILE_SIZE); //Grass ground
		block[1] = bs.grabImage(8, 2, TILE_SIZE, TILE_SIZE); //Earth ground
		block[2] = bs.grabImage(1, 2, TILE_SIZE, TILE_SIZE); //Left platform
		block[3] = bs.grabImage(2, 2, TILE_SIZE, TILE_SIZE); //Middle platform
		block[4] = bs.grabImage(3, 2, TILE_SIZE, TILE_SIZE); //Right platform
		block[5] = bs.grabImage(12, 2, TILE_SIZE, TILE_SIZE); //Single platform
		block[6] = bs.grabImage(15, 2, TILE_SIZE, TILE_SIZE); //Dirt bottom
		block[7] = bs.grabImage(6, 1, TILE_SIZE, TILE_SIZE); //Flat platform bottom
		block[8] = bs.grabImage(16, 2, TILE_SIZE, TILE_SIZE); //Flat dirt bottom
		
		scenery[0] = bs.grabImage(2, 1, TILE_SIZE, TILE_SIZE); //Grass scenery
		scenery[1] = bs.grabImage(3, 1, TILE_SIZE, TILE_SIZE); //Flower scenery
		scenery[2] = bs.grabImage(5, 1, TILE_SIZE, TILE_SIZE); //Grass scenery
		scenery[3] = bs.grabImage(8, 1, TILE_SIZE, TILE_SIZE); //Left grass right dirt
		scenery[4] = bs.grabImage(9, 1, TILE_SIZE, TILE_SIZE); //Left dirt right dirt
		scenery[5] = bs.grabImage(10, 1, TILE_SIZE, TILE_SIZE); //Left dirt right grass
		scenery[6] = bs.grabImage(4, 2, TILE_SIZE, TILE_SIZE); //Left grass ending
		scenery[7] = bs.grabImage(6, 2, TILE_SIZE, TILE_SIZE); //Right grass ending
		scenery[8] = bs.grabImage(7, 2, TILE_SIZE, TILE_SIZE); //Left dirt ending
		scenery[9] = bs.grabImage(9, 2, TILE_SIZE, TILE_SIZE); //Right dirt ending
		scenery[10] = bs.grabImage(10, 2, TILE_SIZE, TILE_SIZE); //Left grass ending in dirt
		scenery[11] = bs.grabImage(11, 2, TILE_SIZE, TILE_SIZE); //right grass ending in dirt
		scenery[12] = bs.grabImage(13, 2, TILE_SIZE, TILE_SIZE); //Right/left grass ending in dirt
		scenery[13] = bs.grabImage(14, 2, TILE_SIZE, TILE_SIZE); //Right/left grass endings
		
		movingPlatform[0] = bs.grabImage(18, 2, TILE_SIZE, TILE_SIZE); //Left end moving platform
		movingPlatform[1] = bs.grabImage(19, 2, TILE_SIZE, TILE_SIZE); //Middle moving platform
		movingPlatform[2] = bs.grabImage(20, 2, TILE_SIZE, TILE_SIZE); //Right end moving platform
		
		
		
//		block[0] = bs.grabImage(1, 1, 32, 32);
//		block[1] = bs.grabImage(2, 1, 32, 32);
//		block[2] = bs.grabImage(1, 2, 32, 32);
//		
//		
//		liquid[0] = bs.grabImage(1, 4, 32, 32); //Static water
//		liquid[1] = bs.grabImage(1, 3, 32, 32);	//Water animation
//		liquid[2] = bs.grabImage(2, 3, 32, 32); //Water animation
//		liquid[3] = bs.grabImage(3, 4, 32, 32); //Static lava
//		liquid[4] = bs.grabImage(3, 3, 32, 32); //Lava animation
//		liquid[5] = bs.grabImage(4, 3, 32, 32); //Lava animation
//		
		
		player[0] = ps.grabImage(1, 1, 32, 64); //Idle right
		player[1] = ps.grabImage(2, 1, 32, 64); //
		player[2] = ps.grabImage(3, 1, 32, 64); //Walking right animation
		player[3] = ps.grabImage(4, 1, 32, 64); //
		player[4] = ps.grabImage(5, 1, 32, 64); //
		
		
		player[5] = ps.grabImage(1, 2, 32, 64); //Idle left
		player[6] = ps.grabImage(2, 2, 32, 64); //
		player[7] = ps.grabImage(3, 2, 32, 64); //Walking left animation
		player[8] = ps.grabImage(4, 2, 32, 64); //
		player[9] = ps.grabImage(5, 2, 32, 64); //
		
		player_jump[0] = ps.grabImage(12, 1, 32, 64);//Jump right
		player_jump[1] = ps.grabImage(12, 2, 32, 64);//Jump left
		
		coins[0] = cs.grabImage(1, 1, 22, 20);
		coins[1] = cs.grabImage(2, 1, 22, 20);
		coins[2] = cs.grabImage(3, 1, 22, 20);
		coins[3] = cs.grabImage(4, 1, 22, 20);
		coins[4] = cs.grabImage(5, 1, 22, 20);
		coins[5] = cs.grabImage(6, 1, 22, 20);
		coins[6] = cs.grabImage(7, 1, 22, 20);
		coins[7] = cs.grabImage(8, 1, 22, 20);
		coins[8] = cs.grabImage(9, 1, 22, 20);
		coins[9] = cs.grabImage(10, 1, 22, 20);
		
		randoms[0] = rs.grabImage(1, 1, 32, 64);
		fleas[0] = rs.grabImage(1, 3, 32, 32); //Left falling/stationary
		fleas[1] = rs.grabImage(2, 3, 32, 32); //
		fleas[2] = rs.grabImage(3, 3, 32, 32); //Left walking
		fleas[3] = rs.grabImage(4, 3, 32, 32); //
		fleas[4] = rs.grabImage(5, 3, 32, 32); //
		fleas[5] = rs.grabImage(1, 4, 32, 32); //Right falling/stationary
		fleas[6] = rs.grabImage(2, 4, 32, 32); //
		fleas[7] = rs.grabImage(3, 4, 32, 32); //Right walking
		fleas[8] = rs.grabImage(4, 4, 32, 32); //
		fleas[9] = rs.grabImage(5, 4, 32, 32); //
		
		flyers[0] = rs.grabImage(6, 2, 32, 32); //
		flyers[1] = rs.grabImage(7, 2, 32, 32); //Left walking
		flyers[2] = rs.grabImage(8, 2, 32, 32); //
		flyers[3] = rs.grabImage(9, 2, 32, 32); //
		flyers[8] = rs.grabImage(10, 2, 32, 32); //Left dead
		
		flyers[4] = rs.grabImage(6, 1, 32, 32); //
		flyers[5] = rs.grabImage(7, 1, 32, 32); //Right walking
		flyers[6] = rs.grabImage(8, 1, 32, 32); //
		flyers[7] = rs.grabImage(9, 1, 32, 32); //
		flyers[9] = rs.grabImage(10, 1, 32, 32); //Right dead
		
		flyers[10] = rs.grabImage(11, 1, 32, 32); //CAW.. bubble
		
		
		cannons[0] = rs.grabImage(2, 1, 64, 32); //Left facing cannon
		cannons[1] = rs.grabImage(2, 2, 64, 32); //Right facing cannon
		cannons[2] = rs.grabImage(4, 1, 32, 64); //Up facing cannon
		cannons[3] = rs.grabImage(5, 1, 32, 64); //down facing cannon
		
		
		
		
	}
}
