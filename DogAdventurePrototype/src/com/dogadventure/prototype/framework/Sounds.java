package com.dogadventure.prototype.framework;

import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;

import com.dogadventure.prototype.window.ResourceLoader;

public class Sounds {
	private final int numberOfSounds = 2;
	
	
	private ResourceLoader rl;
	public Clip[] sounds = new Clip[numberOfSounds];
	private FloatControl[] gainControls = new FloatControl[numberOfSounds];
	
	
	
	private Mixer mixer;

	public Sounds() {
		rl = new ResourceLoader();
		Mixer.Info[] mixInfos = AudioSystem.getMixerInfo();
		mixer = AudioSystem.getMixer(mixInfos[0]);
		

		DataLine.Info dataInfo = new DataLine.Info(Clip.class, null);
		for (int i = 0; i < sounds.length; i++) {
			try {
				sounds[i] = (Clip) mixer.getLine(dataInfo);
				
				
			} catch (LineUnavailableException e) {
				e.printStackTrace();
			}
		}

		getStreams();
	}
	


	private void getStreams() {

		try {
	
			sounds[0].open(rl.loadAudioInputStream("/sounds/click1.wav"));
			gainControls[0] = (FloatControl) sounds[0].getControl(FloatControl.Type.MASTER_GAIN);
			gainControls[0].setValue(6f);
			sounds[1].open(rl.loadAudioInputStream("/sounds/mouserelease1.wav"));
			gainControls[1] = (FloatControl) sounds[1].getControl(FloatControl.Type.MASTER_GAIN);
			gainControls[1].setValue(6f);
			
			
			
			//sounds[7].open(rl.loadAudioInputStream("/sounds/level1-step1.wav"));
			//gainControls[6] = (FloatControl) sounds[6].getControl(FloatControl.Type.MASTER_GAIN);
			//gainControls[6].setValue(6f);
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public void resetClip(Clip clip) {
		clip.setFramePosition(0);
	}

}
