package com.dogadventure.prototype.window;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import com.dogadventure.prototype.audio.Audio;
import com.dogadventure.prototype.audio.AudioPlayer;
import com.dogadventure.prototype.framework.GameObject;
import com.dogadventure.prototype.framework.GameState;
import com.dogadventure.prototype.framework.KeyInput;
import com.dogadventure.prototype.framework.MouseInput;
import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.framework.Sounds;
import com.dogadventure.prototype.framework.Texture;
import com.dogadventure.prototype.objects.Player;

public class Game extends Canvas implements Runnable {

	public static final String LEVELS_LOCATION = "/levels/";
	private final int NUMBER_OF_LEVELS = 5;
	
	private final float BG_SCROLL_VELX = -.3f;
	private final float FG_SCROLL_VELX = -.5f;
	private float bg_x;
	private float bg2_x;
	

	private static final long serialVersionUID = 994438014046176410L;
	private boolean running = false;
	private Thread thread;

	private static MainMenu menu;
	private static PauseMenu pMenu;
	private static LevelSelect levelSelect;
	private static Player player;

	private static GameState state = GameState.Menu;

	public static int WIDTH, HEIGHT;
	private Handler[] levels;
	private int[] levelWidths; // Parallel array with levels
	private int[] startx, starty; // Parallel arrays with levels to hold players
									// start locations
	private int currentLevel;

	// private BufferedImage level = null, menuLevel = null;
	private BufferedImage lifeImage;
	private BufferedImage pauseOverlay;
	private BufferedImage background1, background2, menuBackground;
	private BufferedImage volumeOn, volumeOff;

	private Handler menuHandler, currentHandler = null;
	private static Camera cam;
	private static Texture tex;
	private static Sounds sounds;
	private static Transition trans;

	private static boolean musicMuted = false;

	// private static AudioPlayer audioPlayer;

	private void init() {
		WIDTH = getWidth();
		HEIGHT = getHeight();

		tex = new Texture();
		AudioPlayer.loadMusic();
		AudioPlayer.loadSounds();
		trans = new Transition();

		menu = new MainMenu();
		pMenu = new PauseMenu();
		levelSelect = new LevelSelect(NUMBER_OF_LEVELS);

		ResourceLoader loader = new ResourceLoader();
		lifeImage = loader.loadImage("/life.png");
		pauseOverlay = loader.loadImage("/pause_overlay.png");
		background1 = loader.loadImage("/menubg.gif");
		background2 = loader.loadImage("/background_mountains.png");
		menuBackground = loader.loadImage("/menuBackground.png");
		//cloudsForeground = loader.loadImage("/cloudsForeground.png");
		volumeOn = loader.loadImage("/volumeOn2.png");
		volumeOff = loader.loadImage("/volumeOff2.png");
		bg_x = 0;
		bg2_x = 0;
		
		

	
		newGame(1);
	

		// Init menu stuff
		//menuHandler = new Handler();
		//MapLoader mapLoader = new MapLoader();
		//mapLoader.loadMap(LEVELS_LOCATION + "level1.map", menuHandler);

		this.addMouseListener(new MouseInput());
		AudioPlayer.playMusic(Audio.MENU_MUSIC);
	}

	public synchronized void start() {
		if (running)
			return;

		running = true;
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {

		init();
		this.requestFocus();
		System.out.println("Thread has begun");
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		int updates = 0;
		int frames = 0;
		while (running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				tick();
				updates++;
				delta--;
			}
			render();
			frames++;

			if (System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				System.out.println("FPS: " + frames + " TICKS: " + updates);
				frames = 0;
				updates = 0;
			}
		}

	}

	private void tick() {
		if (state == GameState.Game) {
			if (!trans.transitionIsInProgress()) currentHandler.tick();
			
			
			cam.tick(player);
			if (player.isAtEndOfLevel()) {

				if (levels.length > player.getLevel()) {
					// next level
					trans.startTransition();
					player.setAtEndOfLevel(false);
					player.setLevel(player.getLevel() + 1);
					setLevel();
				} else {
					// end of game
					state = GameState.Menu;
				}
			}

		} else if (state == GameState.Menu) {
			//menuHandler.tick();
		}
		if (state != GameState.Pause) {
			bg_x += BG_SCROLL_VELX;
			if (bg_x < -Game.WIDTH) {
				bg_x = 0;
			}
			
//			fg_x += FG_SCROLL_VELX;
//			if (fg_x < -Game.WIDTH) {
//				fg_x = 0;
//			}
		}
		
		cam.getX();
		trans.tick();

	}

	private void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();
		Graphics2D g2d = (Graphics2D) g;
		// ////

		g.setColor(new Color(25, 191, 225));
		// g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(background1, (int)bg_x, 0, null);
		g.drawImage(background1, (int)(Game.WIDTH + bg_x), 0, null);
		if (state != GameState.Menu) g.drawImage(background2, 0, 0, null);
		
		//g.drawImage(cloudsForeground, (int)fg_x, 0, null);
		//g.drawImage(cloudsForeground, (int)(Game.WIDTH + fg_x), 0, null);

		if (state == GameState.Game) {
			g2d.translate(cam.getX(), cam.getY());
			currentHandler.render(g);
			g2d.translate(-cam.getX(), -cam.getY());
			drawHUD(g);

		} else if (state == GameState.Menu) {
			//menuHandler.render(g2d);
			g.drawImage(menuBackground, 0, 0, null);
			menu.render(g);
		} else if (state == GameState.Pause) {
			g2d.translate(cam.getX(), cam.getY());
			currentHandler.render(g);
			g2d.translate(-cam.getX(), -cam.getY());
			drawHUD(g);
			g.drawImage(pauseOverlay, 0, 0, null);
			pMenu.render(g);
		} else if (state == GameState.LevelSelect) {
			levelSelect.render(g);
		}

		drawVolumeIndicator(g);
		trans.render(g);
		// //
		g.dispose();
		bs.show();
	}

	private void drawHUD(Graphics g) {
		Font fnt2 = new Font("showcard gothic", Font.BOLD, 30);
		g.setFont(fnt2);
		g.setColor(Color.darkGray);
		int cc = player.getCoinsCollected();
		String score = "Coins: " + Integer.toString(cc);
		g.drawString(score, 600, 50);
		int lives = player.getLives();
		if (lives > 3) {
			g.drawImage(lifeImage, 600, 60, null);
			g.drawString("x" + Integer.toString(lives), 640, 85);
		} else {
			for (int i = 0; i < lives; i++) {
				g.drawImage(lifeImage, 600 + 40 * i, 60, null);
			}
		}

	}

	private void drawVolumeIndicator(Graphics g) {
		if (musicMuted) {
			g.drawImage(volumeOff, 15, 15, null);
		} else {
			g.drawImage(volumeOn, 15, 15, null);
		}
	}

	private void setLevel() {
		currentLevel = player.getLevel();
		setCurrentHandler(currentLevel);
		cam = new Camera(0, 0, levelWidths[currentLevel - 1]);
		this.addKeyListener(new KeyInput(levels[currentLevel - 1]));
		levels[currentLevel - 1].addObject(player);
		player.setX(startx[currentLevel - 1]);
		player.setY(starty[currentLevel - 1]);
		player.setStartX(startx[currentLevel - 1]);
		player.setStartY(starty[currentLevel - 1]);
		player.setHandler(currentHandler);

	}

	private void setCurrentHandler(int level) {
		currentHandler = levels[level - 1];
	}

	private void initLevels(int numberOfLevels) {
		levels = new Handler[numberOfLevels];
		levelWidths = new int[numberOfLevels];
		startx = new int[numberOfLevels];
		starty = new int[numberOfLevels];
		// ImageLevelLoader levelLoader = new ImageLevelLoader();
		MapLoader mLoader = new MapLoader();
		for (int i = 0; i < numberOfLevels; i++) {
			levels[i] = new Handler();
		
			mLoader.loadMap(LEVELS_LOCATION + "level" + Integer.toString(i + 1)
					+ ".map", levels[i]);
			levelWidths[i] = mLoader.getMapWidth();
			System.out.printf("%d\n", levelWidths[i]);
			LinkedList<GameObject> object = levels[i].objects;
			for (int j = 0; j < object.size(); j++) {
				GameObject temp = object.get(j);
				if (temp.getId() == ObjectId.Player) {
					startx[i] = ((Player) temp).getStartX();
					starty[i] = ((Player) temp).getStartY();
					System.out.printf("%d %d\n", startx[i], starty[i]);
					levels[i].removeObject(temp);
					break;
				}
			}
		}
	}

	public void newGame(int startLevel) {
		initLevels(NUMBER_OF_LEVELS);
		newPlayer(startLevel);
		setLevel();
	}

	public void newPlayer(int level) {
		player = new Player(0, 0, level, null, ObjectId.Player);
	}

	public static void toggleMusic() {
		musicMuted = !musicMuted;
		if (musicMuted) {
			AudioPlayer.muteSound();
		} else {
			AudioPlayer.unmuteSound();
		}
	}

	public static Camera getCamera() {
		return cam;
	}

	public static Transition getTransition() {
		return trans;
	}

	public static MainMenu getMenu() {
		return menu;
	}

	public static PauseMenu getPauseMenu() {
		return pMenu;
	}

	public static Texture getInstance() {
		return tex;
	}

	public static Sounds getSounds() {
		return sounds;
	}
	
	public static LevelSelect getLevelSelect() {
		return levelSelect;
	}

	public static void setState(GameState s) {
		state = s;
	}

	public static GameState getState() {
		return state;
	}

	public static Player getPlayer() {
		return player;
	}

	public static void main(String[] args) {
		new Window(800, 600, "Dog Adventure Prototype", new Game());
	}

}
