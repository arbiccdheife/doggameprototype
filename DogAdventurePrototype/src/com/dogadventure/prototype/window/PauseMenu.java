package com.dogadventure.prototype.window;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class PauseMenu {
	
	private boolean resumeIsPressed;
	private boolean levelSelectIsPressed;
	private boolean quitIsPressed;
	
	private ResourceLoader loader;
	private BufferedImage buttonUnpressed;
	private BufferedImage buttonPressed;

	public PauseMenu() {
		loader = new ResourceLoader();
		buttonUnpressed = loader.loadImage("/button.png");
		buttonPressed = loader.loadImage("/button_pressed.png");
	}
	
	public void render(Graphics g) {
		Font fnt1 = new Font("showcard gothic", Font.BOLD, 45);
		g.setFont(fnt1);
		g.setColor(Color.darkGray);
		g.drawString("PAUSED", Game.WIDTH/4 + 110, 125);
		
		//g2d.draw(playButton);
		Font fnt2 = new Font("showcard gothic", Font.BOLD, 30);
		g.setFont(fnt2);
		if (!resumeIsPressed) {
			g.drawImage(buttonUnpressed, 310, 200, null);
			g.drawString("RESUME", 342, 235);
		} else {
			g.drawImage(buttonPressed, 310, 204, null);
			g.drawString("RESUME", 342, 239);
		}
		
		if (!levelSelectIsPressed) {
			g.drawImage(buttonUnpressed, 310, 300, null);
			g.drawString("SELECT", 350, 335);
		} else {
			g.drawImage(buttonPressed, 310, 304, null);
			g.drawString("SELECT", 350, 339);
		}
		
		if (!quitIsPressed) {
			g.drawImage(buttonUnpressed, 310, 400, null);
			g.drawString("QUIT", 365, 435);
		} else {
			g.drawImage(buttonPressed, 310, 404, null);
			g.drawString("QUIT", 365, 439);
		}
		
		
		
		
		
		
	}

	public boolean isQuitIsPressed() {
		return quitIsPressed;
	}

	public void setQuitIsPressed(boolean quitIsPressed) {
		this.quitIsPressed = quitIsPressed;
	}

	public boolean isResumeIsPressed() {
		return resumeIsPressed;
	}

	public void setResumeIsPressed(boolean resumeIsPressed) {
		this.resumeIsPressed = resumeIsPressed;
	}
	
	public void setLevelSelectIsPressed(boolean levelSelectIsPressed) {
		this.levelSelectIsPressed = levelSelectIsPressed;
	}
	
	public boolean levelSelectIsPressed() {
		return levelSelectIsPressed;
	}
}
