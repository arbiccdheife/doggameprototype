package com.dogadventure.prototype.window;

import java.io.BufferedReader;

import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.objects.Block;
import com.dogadventure.prototype.objects.Coin;
import com.dogadventure.prototype.objects.Flea;
import com.dogadventure.prototype.objects.FleaCannon;
import com.dogadventure.prototype.objects.Flyer;
import com.dogadventure.prototype.objects.LevelEnd;
import com.dogadventure.prototype.objects.Liquid;
import com.dogadventure.prototype.objects.MobBlocker;
import com.dogadventure.prototype.objects.MovingPlatform;
import com.dogadventure.prototype.objects.Player;
import com.dogadventure.prototype.objects.Scenery;

public class MapLoader {
	private final int TILE_SIZE = 32;

	private BufferedReader br;
	
	private int mapWidth;
	private int mapHeight;
	
	
	
	
//	try {
//		InputStream in = getClass().getResourceAsStream("/testmap.map");
//		BufferedReader br = new BufferedReader(new InputStreamReader(in));
//		int mapWidth = Integer.parseInt(br.readLine());
//		int mapHeight = Integer.parseInt(br.readLine());;
//		int[][] map = new int[mapHeight][mapWidth];
//		String delim = "\\s+";
//		for(int row = 0; row < mapHeight; row++) {
//			String line = br.readLine();
//			String[] tokens = line.split(delim);
//			for(int col = 0; col < mapWidth; col++) {
//				map[row][col] = Integer.parseInt(tokens[col]);
//				System.out.printf("row: %d column: %d value: %d\n", row, col, map[row][col]);
//			}
//		}
//	} catch (Exception e) {
//		e.printStackTrace();
//	}
	
	public int getMapWidth() {
		return mapWidth;
	}


	public int getMapHeight() {
		return mapHeight;
	}
	
	public int getTileSize() {
		return TILE_SIZE;
	}


	public void loadMap(String s, Handler handler) {
		try {
			System.out.printf("%s\n", s);
		ResourceLoader rl = new ResourceLoader();
		br = rl.getBufferedReader(s);
		
		mapWidth = Integer.parseInt(br.readLine());
		mapHeight = Integer.parseInt(br.readLine());
		String delim = "\\s+";
		
		for (int row = 0; row < mapHeight; row++) {
			String line = br.readLine();
			String[] tokens = line.split(delim);
			for (int col = 0; col < mapWidth; col++) {
				addObject(Integer.parseInt(tokens[col]), handler, row, col);
			}
		}
		} catch (Exception e) { e.printStackTrace(); }
		
		
	}
	
	/*TYPES:
	 * 0-grass scenery
	 * 1-flower
	 * 2-grass scenery
	 * 3-left grass ending right dirt ending
	 * 4-left dirt ending right dirt ending
	 * 5-left dirt ending right grass ending
	 * 6-left grass ending
	 * 7-right grass ending
	 * 8-left dirt ending
	 * 9-right dirt ending
	 * 10-left grass ending in dirt
	 * 11-right grass ending in dirt
	 * 12-right/left grass ending in dirt
	 * 13-right/left grass endings
	 * 
	 */
	
	private void addObject(int value, Handler handler, int y, int x) {
		if (value == 0) return;
		switch (value) {
		case(20) :
			//left platform
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 2, ObjectId.Block));
			break;
		case(21) :
			//middle platform
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 3, ObjectId.Block));
			break;
		case(22) :
			//right platform
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 4, ObjectId.Block));
			break;
		case(23) :
			//Left grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 6, ObjectId.Scenery));
			break;
		case(24) :
			//Grass ground
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 0, ObjectId.Block));
			break;
		case(25) :
			//Right grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 7, ObjectId.Scenery));
			break;
		case(26) :
			//Left cliff ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 8, ObjectId.Scenery));
			break;
		case(27) :
			//Dirt block
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 1, ObjectId.Block));
			break;
		case(28) :
			//Right cliff ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 9, ObjectId.Scenery));
			break;
		case(29) :
			//Block w/ left grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 10, ObjectId.Scenery));
			break;
		case(30) :
			//Block w/ right grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 11, ObjectId.Scenery));
			break;
		case(31) :
			//Single platform
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 5, ObjectId.Block));
			break;
		case(32) :
			//Block w/ left/right grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 12, ObjectId.Scenery));
			break;
		case(33) :
			//Air with left/right grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 13, ObjectId.Scenery));
			break;
		case(34) :
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 6, ObjectId.Block));
			break;
		case(35) :
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 8, ObjectId.Block));
			break;
		case(37) :
			//Length 3 moving platform
			handler.addObject(new MovingPlatform(x*TILE_SIZE, y*TILE_SIZE, 3, ObjectId.MovingPlatform, handler));
			break;
		case(1) :
			//grass scenery
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 0, ObjectId.Scenery));
			break;
		case(2) :
			//Flower scenery
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 1, ObjectId.Scenery));
			break;
		case(3) :
			//Player
			Player p = new Player(x * TILE_SIZE, y * TILE_SIZE, 0, handler, ObjectId.Player);
			p.setStartX(x*TILE_SIZE);
			p.setStartY(y*TILE_SIZE);
			handler.addObject(p);
			break;
		case(4) :
			//Grass2 scenery
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 2, ObjectId.Scenery));
			break;
		case(5) :
			//Flat bottomed platform for cannon
			handler.addObject(new Block(x * TILE_SIZE, y * TILE_SIZE, 7, ObjectId.Block));
			break;
		case(7) :
			//Left grass ending with right dirt ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 3, ObjectId.Scenery));
			break;
		case(8) :
			//Left/right dirt ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 4, ObjectId.Scenery));
			break;
		case(9) :
			//Left dirt ending with right grass ending
			handler.addObject(new Scenery(x * TILE_SIZE, y * TILE_SIZE, 5, ObjectId.Scenery));
			break;
		case(10) :
			//Coin
			handler.addObject(new Coin(x*TILE_SIZE+5, y*TILE_SIZE+10, ObjectId.Coin));
			break;
		case(11) :
			//Flyer
			handler.addObject(new Flyer(x*TILE_SIZE+5, y*TILE_SIZE+10, 0, ObjectId.Flyer, handler));
			break;
		case(12) :
			//Left facing flea cannon
			handler.addObject(new FleaCannon(x* TILE_SIZE, y*TILE_SIZE,0, ObjectId.FleaCannon, handler));
			break;
		case(13) :
			//Right facing flea cannon
			handler.addObject(new FleaCannon(x* TILE_SIZE, y*TILE_SIZE, 1, ObjectId.FleaCannon, handler));
			break;
		case(14) :
			//Upward facing flea cannon
			handler.addObject(new FleaCannon(x* TILE_SIZE, y*TILE_SIZE, 2, ObjectId.FleaCannon, handler));
			break;
		case(15) :
			//Downward facing flea cannon
			handler.addObject(new FleaCannon(x* TILE_SIZE, y*TILE_SIZE, 3, ObjectId.FleaCannon, handler));
			break;
		case(16) :
			handler.addObject(new Flea(x*TILE_SIZE, y*TILE_SIZE, 0, ObjectId.Flea, handler));
			break;
		case(17) :
			handler.addObject(new MobBlocker(x*TILE_SIZE, y*TILE_SIZE, ObjectId.MobBlocker));
			break;
		case(18) :
			handler.addObject(new Liquid(x*TILE_SIZE,  y*TILE_SIZE, 4, ObjectId.Liquid));
			break;
		case(19) :
			handler.addObject(new LevelEnd(x*TILE_SIZE, y*TILE_SIZE, ObjectId.LevelEnd));
			break;
		default :
			return;
		}
	}
}
