package com.dogadventure.prototype.window;

import java.awt.image.BufferedImage;

import com.dogadventure.prototype.framework.ObjectId;
import com.dogadventure.prototype.objects.Block;
import com.dogadventure.prototype.objects.Coin;
import com.dogadventure.prototype.objects.Flea;
import com.dogadventure.prototype.objects.LevelEnd;
import com.dogadventure.prototype.objects.Liquid;
import com.dogadventure.prototype.objects.MovingPlatform;
import com.dogadventure.prototype.objects.Player;

public class ImageLevelLoader {
	private int w;
	private int h;
	int maxX = 0;
	
	public void loadImageLevel(BufferedImage image, Handler handler) {
		w = image.getWidth();
		h = image.getHeight();
		
		
		System.out.printf("%d %d\n", w, h);
		
		for (int xx = 0; xx < w; xx++) {
			for (int yy = 0; yy < h; yy++) {
				int pixel = image.getRGB(xx, yy);
				int red = (pixel >> 16) & 0xff;
				int green = (pixel >> 8) & 0xff;
				int blue = pixel & 0xff;
				
				if (red == 255 && green == 255 && blue == 255) { //Dirt block
					handler.addObject(new Block(xx*32, yy*32, 0, ObjectId.Block));
					maxX = xx;
				}
				if (red == 0 && green == 255 && blue == 0) {
					handler.addObject(new Block(xx*32, yy*32, 1, ObjectId.Block));
					maxX = xx;
				}	
				
				if (red == 100 && green == 100 && blue == 100) {
					handler.addObject(new Block(xx*32, yy*32, 2, ObjectId.Block));
					maxX = xx;
				}
				
				if (red == 0 && green == 100 && blue == 255) { //Static water
					handler.addObject(new Liquid(xx*32, yy*32, 0, ObjectId.Liquid));
					maxX = xx;
				}
				if (red == 0 && green == 200 && blue == 255) { //Moving water
					handler.addObject(new Liquid(xx*32, yy*32, 1, ObjectId.Liquid));
					maxX = xx;
				}
				if (red == 255 && green == 100 && blue == 0) { //Static lava
					handler.addObject(new Liquid(xx*32, yy*32, 2, ObjectId.Liquid));
					maxX = xx;
				}
				if (red == 255 && green == 200 && blue == 0) { //Moving lava
					handler.addObject(new Liquid(xx*32, yy*32, 3, ObjectId.Liquid));
					maxX = xx;
				}
				if (red == 220 && green == 220 && blue == 220) { //Bottomless pit
					handler.addObject(new Liquid(xx*32, yy*32, 4, ObjectId.Liquid));
					maxX = xx;
				}
				if (red == 25 && green == 25 && blue == 50) { //Length 3 moving platform
					handler.addObject(new MovingPlatform(xx*32, yy*32, 3, ObjectId.MovingPlatform, handler));
					maxX = xx;
				}
				if (red == 255 && green == 255 && blue == 0) { //Coin
					handler.addObject(new Coin(xx*32+5, yy*32+10, ObjectId.Coin));
					maxX = xx;
				}
				if (red == 0 && green == 240 && blue == 255) { //LevelEnd
					handler.addObject(new LevelEnd(xx*32, yy*32, ObjectId.LevelEnd));
					maxX = xx;
				}
				if (red == 255 && green == 0 && blue == 0) { // Slow Flea
					handler.addObject(new Flea(xx*32, yy*32, 0, ObjectId.Flea, handler));
					maxX = xx;
				}
				if (red == 0 && green == 0 && blue == 255) {
					Player player = new Player(xx*32, yy*32, 0, handler, ObjectId.Player);
					player.setStartX(xx*32);
					player.setStartY(yy*32);
					handler.addObject(player);
				}
			}
		}
		
	}
	
	public int getMaxX() {
		return maxX;
	}
}
