package com.dogadventure.prototype.window;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class LevelSelect {
	private int numberOfLevels;
	private int currentPage = 1;
	private int numberOfPages;

	private boolean firstIsPressed, secondIsPressed, thirdIsPressed;
	private boolean rightArrowPressed, leftArrowPressed;

	private BufferedImage buttonUnpressed;
	private BufferedImage buttonPressed;

	private BufferedImage[] levelThumbnails;

	private BufferedImage arrowLeft, arrowRight;

	public LevelSelect(int numberOfLevels) {
		this.numberOfLevels = numberOfLevels;

		if (this.numberOfLevels % 3 == 0) {
			this.numberOfPages = this.numberOfLevels / 3;
		} else {
			this.numberOfPages = this.numberOfLevels / 3 + 1;
		}

		levelThumbnails = new BufferedImage[numberOfLevels];

		ResourceLoader loader = new ResourceLoader();
		buttonUnpressed = loader.loadImage("/button.png");
		buttonPressed = loader.loadImage("/button_pressed.png");
		arrowLeft = loader.loadImage("/arrowBlue_left.png");
		arrowRight = loader.loadImage("/arrowBlue_right.png");

		for (int i = 0; i < levelThumbnails.length; i++) {
			levelThumbnails[i] = loader.loadImage("/level"
					+ Integer.toString(i + 1) + "thumb.png");
		}
	}

	public void render(Graphics g) {
		Font fnt1 = new Font("showcard gothic", Font.BOLD, 45);
		g.setFont(fnt1);
		g.setColor(Color.darkGray);
		g.drawString("LEVEL SELECT", Game.WIDTH / 4 + 45, 125);
		
		g.fillRect(90, 194, Game.WIDTH - 180, Game.HEIGHT - 343);

		Font fnt2 = new Font("showcard gothic", Font.BOLD, 30);
		g.setFont(fnt2);
		if (!firstIsPressed) {
			g.drawImage(buttonUnpressed, 100, 400, null);
			g.drawString(Integer.toString((currentPage - 1) * 3 + 1), 190, 435);
		} else {
			g.drawImage(buttonPressed, 100, 404, null);
			g.drawString(Integer.toString((currentPage - 1) * 3 + 1), 190, 439);
		}

		g.drawImage(levelThumbnails[(currentPage - 1) * 3], 100, 204, null);
		
		if ((currentPage - 1) * 3 + 2 <= numberOfLevels) {
			if (!secondIsPressed) {
				g.drawImage(buttonUnpressed, 310, 400, null);
				g.drawString(Integer.toString((currentPage - 1) * 3 + 2), 400,
						435);
			} else {
				g.drawImage(buttonPressed, 310, 404, null);
				g.drawString(Integer.toString((currentPage - 1) * 3 + 2), 400,
						439);
			}
			
			g.drawImage(levelThumbnails[(currentPage - 1) * 3 + 1], 310, 204, null);
		}

		if ((currentPage - 1) * 3 + 3 <= numberOfLevels) {
			if (!thirdIsPressed) {
				g.drawImage(buttonUnpressed, 520, 400, null);
				g.drawString(Integer.toString((currentPage - 1) * 3 + 3), 605,
						435);
			} else {
				g.drawImage(buttonPressed, 520, 404, null);
				g.drawString(Integer.toString((currentPage - 1) * 3 + 3), 605,
						439);
			}
			g.drawImage(levelThumbnails[(currentPage - 1) * 3 + 2], 520, 204, null);
		}

		if (currentPage < numberOfPages) {
			// g.fillRect(750, 300, 30, 30);
			g.drawImage(arrowRight, 750, 300, null);
		}
		if (currentPage > 1) {
			// g.fillRect(50, 300, 30, 30);
			g.drawImage(arrowLeft, 50, 300, null);
		}

		// Graphics2D g2d = (Graphics2D)g;
		// g2d.setColor(Color.red);
		// g2d.draw(new Rectangle(0,0,Game.WIDTH/2,Game.HEIGHT));
		// g2d.draw(new Rectangle(0,0,Game.WIDTH/4,Game.HEIGHT));
		// g2d.draw(new Rectangle(0,0,(Game.WIDTH/4)*3,Game.HEIGHT));

	}

	public int getNumberOfLevels() {
		return numberOfLevels;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int page) {
		currentPage = page;
	}

	public int getNumberOfPages() {
		return numberOfPages;
	}

	public boolean firstButtonPressed() {
		return firstIsPressed;
	}

	public boolean secondButtonPressed() {
		return secondIsPressed;
	}

	public boolean thirdButtonPressed() {
		return thirdIsPressed;
	}

	public void setFirstButtonPressed(boolean tf) {
		firstIsPressed = tf;
	}

	public void setSecondButtonPressed(boolean tf) {
		secondIsPressed = tf;
	}

	public void setThirdButtonPressed(boolean tf) {
		thirdIsPressed = tf;
	}

	public boolean leftButtonPressed() {
		return leftArrowPressed;
	}

	public boolean rightButtonPressed() {
		return rightArrowPressed;
	}

	public void setLeftButtonPressed(boolean tf) {
		leftArrowPressed = tf;
	}

	public void setRightButtonPressed(boolean tf) {
		rightArrowPressed = tf;
	}

}
