package com.dogadventure.prototype.window;

import java.awt.Graphics;
import java.util.LinkedList;

import com.dogadventure.prototype.framework.GameObject;

public class Handler {
	
	public LinkedList<GameObject> objects = new LinkedList<GameObject>();
	
	private GameObject tempObject;

	public void tick() {
		for (int i = 0; i < objects.size(); i++) {
			tempObject = objects.get(i);
			tempObject.tick(objects);
		}
	}
	
	public void render(Graphics g) {
		Camera cam = Game.getCamera();
		
		float currentX = cam.getX();
		
		for (int i = 0; i < objects.size(); i++) {
			tempObject = objects.get(i);
			
			if (tempObject.getX() >= currentX - Game.WIDTH )
				tempObject.render(g);
			//else if (r == null) tempObject.render(g);
		}
	}
	
	public void addObject(GameObject object) {
		this.objects.add(object);
	}
	
	public void removeObject(GameObject object) {
		this.objects.remove(object);
	}
	

}
