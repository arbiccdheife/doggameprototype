package com.dogadventure.prototype.window;

import java.awt.Color;
import java.awt.Graphics;

public class Transition {
	private final int TRANSITION_TIME = 1500; //milliseconds
	
	private boolean transitionInProgress = false;
	private long transitionBeginTime = 0;
	
	
	private double percentageComplete = 0;
	
	
	public void tick() {
		if (transitionIsInProgress()) {
			long now = System.currentTimeMillis();
			percentageComplete = (double)(now - transitionBeginTime) / TRANSITION_TIME;
			if (now - transitionBeginTime >= TRANSITION_TIME ) {
				transitionInProgress = false;
				transitionBeginTime = 0;
			}
		}
		
	}
	
	public void render(Graphics g) {
		
		if (transitionIsInProgress()) {
			g.setColor(Color.black);
			int x = (int)(0 + (Game.WIDTH/2) * percentageComplete);
			int y = (int)(0 + (Game.HEIGHT/2) * percentageComplete);
			g.fillRect(x, y, (Game.WIDTH/2 - x) * 2, (Game.HEIGHT/2 - y)*2);
		}
		
	}
	
	public void startTransition() {
		transitionInProgress = true;
		transitionBeginTime = System.currentTimeMillis();
	}
	
	public boolean transitionIsInProgress() {
		return transitionInProgress;
	}
}
