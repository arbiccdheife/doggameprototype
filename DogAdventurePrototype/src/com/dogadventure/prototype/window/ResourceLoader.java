package com.dogadventure.prototype.window;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

public class ResourceLoader {

	private BufferedImage image;
	private AudioInputStream audioStream;

	public BufferedImage loadImage(String path) {
		try {
			image = ImageIO.read(getClass().getResource(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return image;
	}

	public AudioInputStream loadAudioInputStream(String path) {
		URL soundURL = getClass().getResource(path);
		try {
			audioStream = AudioSystem.getAudioInputStream(soundURL);
		} catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return audioStream;
	}

	public BufferedReader getBufferedReader(String path) {
		BufferedReader br = null;
		try {
		InputStream in = getClass().getResourceAsStream(path);
		br = new BufferedReader(new InputStreamReader(in));
		} catch (Exception e) { e.printStackTrace(); }
		return br;
	}
}
