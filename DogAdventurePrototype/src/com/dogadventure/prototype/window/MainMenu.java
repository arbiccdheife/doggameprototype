package com.dogadventure.prototype.window;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

public class MainMenu {
	public Rectangle testButton = new Rectangle(0, 0, Game.WIDTH/2, Game.HEIGHT);
	public Rectangle testButton2 = new Rectangle(0, 0, Game.WIDTH/2-95, Game.HEIGHT);
	
	private ResourceLoader loader;
	private BufferedImage buttonUnpressed;
	private BufferedImage buttonPressed;
	
	private boolean playIsPressed = false;
	private boolean levelSelectIsPressed = false;
	private boolean quitIsPressed = false;
	


	public void setLevelSelectIsPressed(boolean levelSelectIsPressed) {
		this.levelSelectIsPressed = levelSelectIsPressed;
	}
	
	public void setPlayIsPressed(boolean playIsPressed) {
		this.playIsPressed = playIsPressed;
	}


	public void setQuitIsPressed(boolean quitIsPressed) {
		this.quitIsPressed = quitIsPressed;
	}


	public MainMenu() {
		loader = new ResourceLoader();
		buttonUnpressed = loader.loadImage("/button.png");
		buttonPressed = loader.loadImage("/button_pressed.png");
	}
	
	
	public void render(Graphics g) {
		Font fnt1 = new Font("showcard gothic", Font.BOLD, 45);
		g.setFont(fnt1);
		g.setColor(Color.darkGray);
		g.drawString("DOG ADVENTURE", Game.WIDTH/4, 125);
		
		//g2d.draw(playButton);
		Font fnt2 = new Font("showcard gothic", Font.BOLD, 30);
		g.setFont(fnt2);
		if (!playIsPressed) {
			g.drawImage(buttonUnpressed, 310, 200, null);
			g.drawString("PLAY", 365, 235);
		} else {
			g.drawImage(buttonPressed, 310, 204, null);
			g.drawString("PLAY", 365, 239);
		}
		if (!levelSelectIsPressed) {
			g.drawImage(buttonUnpressed, 310, 300, null);
			g.drawString("SELECT", 350, 335);
		} else {
			g.drawImage(buttonPressed, 310, 304, null);
			g.drawString("SELECT", 350, 339);
		}
		if (!quitIsPressed) {
			g.drawImage(buttonUnpressed, 310, 400, null);
			g.drawString("QUIT", 365, 435);
		} else {
			g.drawImage(buttonPressed, 310, 404, null);
			g.drawString("QUIT", 365, 439);
		}
		
		
		
		
		
		
	}

	public boolean levelSelectIsPressed() {
		return levelSelectIsPressed;
	}
	
	public boolean isPlayIsPressed() {
		return playIsPressed;
	}


	public boolean isQuitIsPressed() {
		return quitIsPressed;
	}
}
