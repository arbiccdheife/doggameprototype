package com.dogadventure.prototype.window;

import com.dogadventure.prototype.framework.GameObject;

public class Camera {
	private float x, y;
	//private int levelWidth;
	
	private float tween = .025f;
	
	private int xmin, xmax;
	
	
	public Camera(float x, float y, int levelWidth) {
		this.x = x;
		this.y = y;
		
		xmin = Game.WIDTH - levelWidth;
		xmax = 0;
		
		System.out.printf("xmin: %d\n", xmin);
		//this.levelWidth = levelWidth;
		
		System.out.println(levelWidth);
	}
	
	public void tick(GameObject player) {
		x += ((-player.getX()+Game.WIDTH/2)-x) * .025f;
		//x = (player.getX() - x) * tween;
		fixBounds();
	}
	private void fixBounds() {
		if (x > xmin) x = xmin;
		if (x > xmax) x = xmax;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
}
