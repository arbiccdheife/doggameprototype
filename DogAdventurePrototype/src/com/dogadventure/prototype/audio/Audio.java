package com.dogadventure.prototype.audio;

public class Audio {
	public static final String JUMP = "jump";
	public static final String COIN = "coin";
	public static final String BLAST = "blast";
	public static final String CAW = "caw";
	public static final String BUTTON_PRESS = "buttonpress";
	public static final String BUTTON_RELEASE = "buttonrelease";
	
	
	public static final String MENU_MUSIC = "menumusic";
}
