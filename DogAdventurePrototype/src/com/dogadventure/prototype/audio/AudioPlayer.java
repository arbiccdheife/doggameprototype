package com.dogadventure.prototype.audio;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.openal.SoundStore;

import com.dogadventure.prototype.framework.Sounds;

public class AudioPlayer {
	public static final String SOUND_LOCATION = "sounds/";
	
	private static final float SOUND_VOLUME = .4f;
	private static final float MUSIC_VOLUME = 10f;
	
	private static Map<String, Sound> soundMap = new HashMap<String, Sound>();

	private static Map<String, Music> musicMap = new HashMap<String, Music>();
	
	
	private static Sounds  wavSounds= new Sounds();
	
	public static void addSound(String key, String path) {
		try {
			soundMap.put(key, new Sound(SOUND_LOCATION + path));
		} catch (SlickException e) {
			e.printStackTrace();
			//System.out.printf("%s not loading\n", path);
			System.exit(0);
		}
	}

	
	public static Sound getSound(String key) {
		return soundMap.get(key);
	}
	
	public static void loadSounds() {
		addSound(Audio.JUMP, "Jump.ogg");
		addSound(Audio.COIN, "Pickup_Coin.ogg");
		addSound(Audio.BLAST, "Explosion.ogg");
		addSound(Audio.CAW, "birdCaw.ogg");
		
		SoundStore.get().setSoundVolume(SOUND_VOLUME);
		//addSound(Audio.BUTTON_RELEASE, "mouserelease1.ogg");
		//addSound(Audio.BUTTON_PRESS, "click1.ogg");
		
	}
	
	public static void playSound(String key) {
		if (soundMap.get(key) != null) {
			soundMap.get(key).play();
		} else {
			if (key.equals(Audio.BUTTON_PRESS)) {
				wavSounds.resetClip(wavSounds.sounds[0]);
				wavSounds.sounds[0].start();
			} else if (key.equals(Audio.BUTTON_RELEASE)) {
				wavSounds.resetClip(wavSounds.sounds[1]);
				wavSounds.sounds[1].start();
			}
		}
	}
	
	
	public static Music getMusic(String key) {
		return musicMap.get(key);
		
		
	}
	
	public static void addMusic(String key, String path) {
	
		try {
			musicMap.put(key, new Music(SOUND_LOCATION + path));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void loadMusic() {
		addMusic(Audio.MENU_MUSIC, "spiritwaltz.ogg");
		SoundStore.get().setMusicVolume(10f);
	}
	
	public static void playMusic(String key) {
		Music m = musicMap.get(key);
		if (m != null) {
			m.loop();
		}
	}
	
	public static void muteSound() {
		SoundStore.get().setMusicVolume(0);
		SoundStore.get().setSoundVolume(0);
	}
	
	public static void unmuteSound() {
		SoundStore.get().setMusicVolume(MUSIC_VOLUME);
		SoundStore.get().setSoundVolume(SOUND_VOLUME);
	}

}
